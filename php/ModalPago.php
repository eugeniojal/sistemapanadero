<div class="modal fade" id="nuevoCliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
		  	<h4 class="modal-title" id="myModalLabel"><i class='glyphicon glyphicon-edit'></i>Detalles del pago</h4>
			<button type="button" class="close float-left" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			
		  </div>
		  <div class="modal-body">
		  	<div id="detalle"></div>
		
		</div>
		 <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		  </div>

	  </div>
	</div>
</div>