<?php 

	session_start();
	if (!isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] != 1) {
        header("location:../");
		exit;
        }
	
include('navbar.php');
?>
<div class="container" style="margin-bottom: 20px;">
	<div class="card">
		<div class="card-header">
			<div class="row">

<div class="col">
<div class="input-group mb-3">
  <input type="text" class="form-control" placeholder="Buscar Producto" aria-label="Recipient's username" aria-describedby="button-addon2" id="Bproducto">
  <div class="input-group-append">
    <button class="btn btn-outline-secondary" type="button" id="button-addon2">Buscar</button>
  </div>
</div>
</div>
			</div>

  </div>
	</div>
		  
	
</div>
<div class="container-fluid" id="div-productos">
<div class="row">
<?php 
include('conexion.php');
$productos = mysqli_query($enlace,"SELECT * FROM productos");
if (mysqli_num_rows($productos) > 0 ) {
  while ($producto = mysqli_fetch_array($productos)) {
?>

  <div class="col">
    <div class="card" style="width: 18rem;margin: 20px;" >
  <img src="<?php echo $producto['adjunto']; ?>" class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title"><?php echo $producto['nombre']; ?></h5>
    <p class="card-text"><strong>Precio:</strong><?php echo number_format($producto['precio'], 2, ',', '.',) ?></p>
    <p class="card-text"><strong>cantidad:</strong><?php echo $producto['cantidad']; ?></p>
    <p class="card-text"><strong>Panaderia:</strong><?php echo $producto['usuario']; ?></p>
    <p class="card-text"><strong>Descripcion:</strong><?php echo $producto['detalle']; ?></p>
    <a href="RegistroP.php?id=<?php echo $producto['id_producto'];?> " class="btn btn-primary">Comprar</a>
  </div>
</div>
  </div>


<?php 
  }
}
?>	
</div>




	
</div>












<?php
include('footer.php');

 ?>