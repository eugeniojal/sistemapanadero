
<?php include('navbar.php');?>
<div class="container">


<div class="card">
	<div class="card-header">
		Registro de Producto
	</div>
		<form class="form-horizontal" method="POST" action="registroProducto.php" enctype="multipart/form-data" autocomplete="off">
	<div class="card-body">
		
<div class="row">
	<div class="col">
		<label for="nombre">Nombre del producto</label>
		<input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre del producto">
		
	</div>
	
</div>
<div class="row">
	<div class="col">
		<label for="precio">Precio</label>
		<input type="text" id="precio" name="precio" class="form-control"  placeholder="Precio">
		
	</div>
	<div class="col">
		<label for="cantidad">cantidad</label>
		<input type="number" class="form-control" id="cantidad" name="cantidad" placeholder="cantidad">
		
	</div>
	
</div>
<div class="row">
	<div class="col">
	  <label for="exampleFormControlTextarea1">Detalles</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="detalles"></textarea>
		
	</div>
	
</div>
<div class="row">
	<div class="col">
	 <label for="archivo">Imagen del producto</label>
    <input type="file" class="form-control-file" id="archivo" name="archivo">
	</div>
	
</div>

	


	</div>
		<div class="card-footer">
				 <button type="submit" class="btn btn-primary mb-2">Crear</button>	
	</div>	
	</form>

</div>

			</div>
<?php include("footer.php"); ?>
