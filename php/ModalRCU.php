<?php    include("conexion.php");?>
<div class="modal fade" id="nuevoCliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">


		<div class="modal-content">

		  <div class="modal-header">
		
			<h4 class=" " id="myModalLabel"><i class='glyphicon glyphicon-edit'></i> Agregar nueva cuenta</h4>
				<button type="button" class="close floatl-rigth" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  </div>

		  <div class="modal-body">

			<form class="form-horizontal" method="post" id="guardar_cliente" name="guardar_cliente">


			<div id="resultados_ajax"></div>



			<div class="form-group">
				<label for="nombre" class="col-sm-6 control-label">Cliente</label>
				<div class="col-sm-12">
			  <select name="cliente" id="cliente" class="form-control input-sm" required>
			<option value="0">Elija un Cliente</option>
			<?php
          $query = mysqli_query($enlace,"SELECT * FROM clientes");
          while ($valores = mysqli_fetch_array($query)) {
            echo '<option value="'.$valores['id_cliente'].'">'.$valores['Nombre'].'</option>';
          }
        ?>
		</select>
	</div>
     </div>
			  <div class="form-group">
				<label for="nombre" class="col-sm-6 control-label">Alias</label>
				<div class="col-sm-12">
				  <input type="text" class="form-control" id="nombre" name="nombre" required>
				</div>
			  </div>


			  <div class="form-group">
				<label for="cedula" class="col-sm-12 control-label">CI Titular</label>
				<div class="col-sm-12">
					<input type="number" class="form-control" id="ci" name="ci" required>
				  
				</div>
			  </div>

			

			  <div class="form-group">
				<label for="banco" class="col-sm-3 control-label">Banco</label>
				<div class="col-sm-12">
				 <select name="banco"  id="banco" name="banco" class="form-control input-sm">
			<option value="0">Seleccione un Banco</option>
			<?php
          $query = mysqli_query($enlace,"SELECT * FROM bancos");
          while ($valores = mysqli_fetch_array($query)) {
            echo '<option value="'.$valores['nombre'].'">'.$valores['nombre'].'</option>';
          }
        ?>
		</select>
				</div>
			  </div>
			  
			  

			  
			  <div class="form-group">
				<div>
					<label for="cuenta" class="col-sm-12 control-label">Numero de Cuenta</label>
				</div>
				<div class="col-sm-12">
					<input type="number" class="form-control" id="cuenta" name="cuenta"   maxlength="20" required> 
				  
				</div>
			  </div>
			  
			 
			
		 
		 </form>
       </div>

		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			<button type="" onclick="javascript:RegistroC();"  class="btn btn-primary" id="guardar_datos">Guardar datos</button>
		  </div>

		  
		</div>
	  </div>
	

	</div>