<?php
require('../fpdf/fpdf.php');
require('conexion.php');
//require('conexion.php');
$desde = $_GET['desde'];
$hasta = $_GET['hasta'];


	# code...
$pdf = new FPDF('L','mm','A4');
$pdf->AddPage();
$pdf->SetFont('Arial', '', 10);
$pdf->SetXY(250, 10);
$pdf->SetFont('Arial', '', 9);
$pdf->Cell(50, 10, 'FECHA: '.date('d-m-Y').'', 0);
$pdf->Ln(10);
$pdf->SetFont('Arial', 'B', 11);
$pdf->SetXY(120, 20);
$pdf->Cell(150, 8, 'DETALLES FACTURACION',0,0,'center');
$pdf->Ln(10);
$pdf->SetXY(115, 25);
$pdf->Ln(10);
$pdf->SetFont('Arial', 'B', 11);
$pdf->SetXY(10, 40);
$pdf->Ln(5);
$pdf->SetFont('Arial', 'B', 8);
$pdf->Rect(10, 47, 275, 5);
$pdf->Cell(50, 8, 'NUMERO DE REFERENCIA', 0);
$pdf->Cell(60, 8, 'BANCO', 0);
$pdf->Cell(100, 8,'CLIENTE', 0);
$pdf->Cell(40, 8,'MONTO', 0);
$pdf->Cell(35, 8, 'FECHA', 0);
$pdf->Ln(5);
$pdf->SetFont('Arial', '', 8);


$productos = mysqli_query($enlace,"SELECT  *FROM pagos WHERE fechaR BETWEEN '$desde' AND '$hasta' ORDER BY id_pago DESC");

$totaluni = 0;
$totaldis = 0;
while($productos2 = mysqli_fetch_array($productos)){
  
	$totaldis = $totaldis + $productos2['monto'];
    $pdf->Cell(10, 8,'', 0);
    $pdf->Cell(40, 8,$productos2['NumRef'], 0);
	$pdf->Cell(60, 8, $productos2['banco'], 0);
	$pdf->Cell(100,8,$productos2['Cliente'], 0);
	$pdf->Cell(35, 8,number_format($productos2['monto'],2,',','.'), 0);
	$pdf->Cell(25, 8,$productos2['fechaR'], 0);
	$pdf->Ln(4);
}
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(200,8,'',0);
$pdf->Cell(80,8,'Total:'.number_format($totaldis,2,',','.'),0);
$pdf->Output('REPORTE TODOS LOS PAGOS .pdf','I');

?>
