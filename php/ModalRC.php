<div class="modal fade" id="nuevoCliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">


		<div class="modal-content">

		  <div class="modal-header">
		
			<h4 class=" " id="myModalLabel"><i class='glyphicon glyphicon-edit'></i> Agregar nuevo cliente</h4>
				<button type="button" class="close floatl-rigth" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  </div>

		  <div class="modal-body">

			<form class="form-horizontal" method="post" id="guardar_cliente" name="guardar_cliente">


			<div id="resultados_ajax"></div>

			  <div class="form-group">
				<label for="nombre" class="col-sm-6 control-label">Nombre y apellido</label>
				<div class="col-sm-12">
				  <input type="text" class="form-control" id="nombre" name="nombre" required>
				</div>
			  </div>


			  <div class="form-group">
				<label for="email" class="col-sm-3 control-label">Cedula</label>
				<div class="col-sm-12">
					<input type="text" class="form-control" id="rif" name="email" required >
				  
				</div>
			  </div>

			

			  <div class="form-group">
				<label for="telefono" class="col-sm-3 control-label">Telefono</label>
				<div class="col-sm-12">
				  <input type="text" class="form-control" id="tlf" name="telefono" >
				</div>
			  </div>
			  
			  

			  
			  <div class="form-group">
				<div>
					<label for="direccion" class="col-sm-3 control-label">Dirección</label>
				</div>
				<div class="col-sm-12">
					<textarea class="form-control" id="direc" name="direccion"   maxlength="255" ></textarea>
				  
				</div>
			  </div>
			  
			 
			
		 
		 </form>
       </div>

		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			<button type="" onclick="javascript:Registro();"  class="btn btn-primary" id="guardar_datos">Guardar datos</button>
		  </div>

		  
		</div>
	  </div>
	

	</div>