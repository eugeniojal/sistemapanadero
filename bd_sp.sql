-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-01-2020 a las 05:39:27
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_sp`
--
CREATE DATABASE IF NOT EXISTS `bd_sp` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `bd_sp`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bancos`
--

CREATE TABLE `bancos` (
  `id_bank` int(11) NOT NULL,
  `nombre` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `bancos`
--

INSERT INTO `bancos` (`id_bank`, `nombre`) VALUES
(1, 'venezuela'),
(2, 'banesco'),
(3, 'provincial');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id_cliente` int(11) NOT NULL,
  `Nombre` varchar(20) NOT NULL,
  `Rif` varchar(10) NOT NULL,
  `tlf` varchar(12) NOT NULL,
  `direc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `Nombre`, `Rif`, `tlf`, `direc`) VALUES
(16, 'eugenio jose', '25897751', ' 258944215', 'asasdas'),
(17, 'Marco Tulio Diaz', '78945556', ' 2565125', 'asdsad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas`
--

CREATE TABLE `cuentas` (
  `id_cuenta` int(10) NOT NULL,
  `Bank` varchar(20) NOT NULL,
  `NumCuenta` varchar(20) NOT NULL,
  `id_cliente` int(10) NOT NULL,
  `Titular` varchar(30) NOT NULL,
  `ci` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `id_pago` int(10) NOT NULL,
  `NumRef` varchar(15) NOT NULL,
  `banco` varchar(20) NOT NULL,
  `monto` float(20,2) NOT NULL,
  `fechaR` date NOT NULL,
  `fechaC` date NOT NULL,
  `id_cliente` int(10) NOT NULL,
  `Cliente` varchar(20) NOT NULL,
  `UsuarioR` varchar(10) NOT NULL,
  `UsuarioC` varchar(10) NOT NULL,
  `adjunto` varchar(250) NOT NULL,
  `Nota` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pagos`
--

INSERT INTO `pagos` (`id_pago`, `NumRef`, `banco`, `monto`, `fechaR`, `fechaC`, `id_cliente`, `Cliente`, `UsuarioR`, `UsuarioC`, `adjunto`, `Nota`) VALUES
(26, '1155', 'provincial', 5555.00, '2021-02-01', '0000-00-00', 0, 'eanton1', '', '', '', '255515'),
(27, '2', 'venezuela', 21.00, '1121-02-12', '0000-00-00', 0, 'eanton1', '', '', '', '21'),
(28, '121', 'provincial', 2121.00, '0112-12-21', '0000-00-00', 0, 'eanton1', '', '', '', '121212'),
(29, '21', 'banesco', 121.00, '0001-02-01', '0000-00-00', 0, 'eanton1', '', '', '', '21121'),
(30, '11', 'banesco', 101.00, '2020-01-15', '0000-00-00', 0, 'eanton1', '', '', '', 'qwqwq');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id_producto` int(10) NOT NULL,
  `usuario` varchar(252) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `cantidad` int(30) NOT NULL,
  `precio` double NOT NULL,
  `detalle` text COLLATE utf8_spanish_ci NOT NULL,
  `adjunto` varchar(350) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_producto`, `usuario`, `nombre`, `cantidad`, `precio`, `detalle`, `adjunto`) VALUES
(1, 'Panaderia El trigo 3000', 'pan-campesino-por-und.jpg', -10, 1.5, 'Pan campesino', 'files//pan-campesino-por-und.jpg'),
(2, 'Panaderia El trigo 3000', 'qwerty.jpg', -10, 1.5, 'pan campesino con harina', 'files/qwerty.jpg'),
(3, 'Panaderia El trigo 3000', 'gatos.jpg', 490, 1500, 'pan campesino loco', 'files/gatos.jpg'),
(4, 'Panaderia El trigo 3000', 'Pan sobado', 190, 25000, 'Pan sobado grande ', 'files/RFB-0907-6-pansobado.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `nombre` varchar(11) NOT NULL,
  `clave` varchar(11) NOT NULL,
  `alias` varchar(350) NOT NULL,
  `tipo` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nombre`, `clave`, `alias`, `tipo`) VALUES
(1, 'admin', 'inicio', 'Panaderia El trigo 3000', 1),
(2, 'mtulio', 'inicio', '', 0),
(3, 'lfreire', 'jose', '', 0),
(4, 'jgimenez', '652546', '', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bancos`
--
ALTER TABLE `bancos`
  ADD PRIMARY KEY (`id_bank`),
  ADD UNIQUE KEY `id_bank` (`id_bank`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `cuentas`
--
ALTER TABLE `cuentas`
  ADD PRIMARY KEY (`id_cuenta`),
  ADD UNIQUE KEY `NumCuenta` (`NumCuenta`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`id_pago`),
  ADD UNIQUE KEY `NumRef` (`NumRef`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_producto`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bancos`
--
ALTER TABLE `bancos`
  MODIFY `id_bank` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `cuentas`
--
ALTER TABLE `cuentas`
  MODIFY `id_cuenta` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `id_pago` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_producto` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
